# -------- SQREEN PROTECTION --------
#import sqreen
# sqreen.start()
# -----------------------------------
import logging
import os
import requests
import json
from flask import Flask, jsonify, request

app = Flask(__name__)
# Configurations
app.config.from_object('config')
config = app.config

# Logger setup

hdlr = logging.FileHandler('error.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
app.logger.addHandler(hdlr)
logger = app.logger

# Logger Reference
# logger.debug(‘this is a DEBUG message’)
# logger.info(‘this is an INFO message’)
# logger.warning(‘this is a WARNING message’)
# logger.error(‘this is an ERROR message’)
# logger.critical(‘this is a CRITICAL message’)


@app.errorhandler(500)
def internal_error(exception):
    logger.error(exception)
    return 'Internal Error', 500


@app.errorhandler(404)
def not_found(error):
    return 'Not Found', 404


@app.route('/', methods=['GET'])  # root route
def root():
    logger.debug('root loaded')
    return 'Server running'

@app.route("/rSweES", methods=["GET"])
def get_my_ip():
    ip = None
    if request.environ.get('HTTP_X_FORWARDED_FOR') is None:
        ip = request.environ['REMOTE_ADDR']
        logger.debug('ip logged %s' % request.environ['REMOTE_ADDR'])
    else:
        ip = request.environ['HTTP_X_FORWARDED_FOR']
        logger.debug('ip logged %s' % request.environ['HTTP_X_FORWARDED_FOR'])


    send_url = 'http://api.ipstack.com/' + ip +'?access_key=' + config['IPSTACK_API']
    r = requests.get(send_url)
    j = json.loads(r.text)
    lat = j['latitude']
    lon = j['longitude']

    logger.debug(j)

    return '( ͡☉ ͜ʖ ͡☉)', 200
